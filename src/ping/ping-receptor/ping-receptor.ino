#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
// Hardware configuration
//Set up nRF24L01 radio on SPI bus plus pins 9 & 10
//RF24 (cepin, cspin)
RF24 radio(9, 10);
//
// Topology
// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
void setup(void)
{
  pinMode(10, OUTPUT);
  Serial.begin(9600);
  // Setup and configure rf radio
  radio.begin();
  // optionally, increase the delay between retries & # of retries
  radio.setRetries(15, 15);
  
  // optionally, reduce the payload size.  seems to
  // improve reliability
  //radio.setPayloadSize(8);
  
  // Start listening
  Serial.println("Iniciando la escucha en 328");
  radio.startListening();
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1, pipes[0]);
}

void loop(void)
{
  Serial.println("Loop en 328 ");
  // if there is data ready
  if ( radio.available() )
  {
    // Dump the payloads until we've gotten everything
    unsigned long got_time;
    bool done = false;
    while (radio.available())
    {
      // Fetch the payload, and see if this was the last one.
      // done = radio.read( &got_time, sizeof(unsigned long) );
      radio.read( &got_time, sizeof(unsigned long) );
      Serial.print("Dato Recibido en 328 =");
      Serial.println(got_time);
      // Delay just a little bit to let the other unit
      // make the transition to receiver
      delay(20);
    }
    
    // First, stop listening so we can talk
    radio.stopListening();
    
    // Send the final one back.
    Serial.println("Enviando Respuesta en 328 ");
    radio.write( &got_time, sizeof(unsigned long) );
        
    // Now, resume listening so we catch the next packets.
    Serial.println("Escuchando 328 ");
    radio.startListening();
  }
}
