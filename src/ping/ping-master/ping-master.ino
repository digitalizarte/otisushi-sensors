/**
   RF24Mesh_Example_Master.ino by TMRh20
   This example sketch shows how to manually configure a node via RF24Mesh as a master node, which
   will receive all data from sensor nodes.

   The nodes can change physical or logical position in the network, and reconnect through different
   routing nodes as required. The master node manages the address assignments for the individual nodes
   in a manner similar to DHCP.
*/

#include "RF24Network.h"
#include "RF24.h"
#include "RF24Mesh.h"
#include <SPI.h>

//Include eeprom.h for AVR (Uno, Nano) etc. except ATTiny
#include <EEPROM.h>
#include <elapsedMillis.h>

elapsedMillis timeElapsed;

/***** Configure the chosen CE,CS pins *****/
RF24 radio(9, 10);
RF24Network network(radio);
RF24Mesh mesh(radio, network);

uint32_t displayTimer = 0;

unsigned int interval = 5000;
unsigned long counter = 0;

struct payload_t {
  unsigned long ms;
  unsigned long counter;
};

void setup() {
  // Enciende la antena
  // Toma la corriente del pin A0.
  // pinMode(A0, OUTPUT);
  // digitalWrite(A0, HIGH);

  //
  Serial.begin(115200);
  Serial.println("master - Setup mesh::setNodeID ");

  // Set the nodeID to 0 for the master node
  mesh.setNodeID(0);
  Serial.print("master - Setup mesh::NodeID: ");
  Serial.print(mesh.getNodeID());
  Serial.println("");

  // Connect to the mesh
  Serial.println("master - Setup mesh::begin");
  mesh.begin();
}

void loop() {
  network.update();
  
  // Call mesh.update to keep the network updated
  mesh.update();

  // In addition, keep the 'DHCP service' running on the master node so addresses will
  // be assigned to the sensor nodes
  mesh.DHCP();

  // Check for incoming data from the sensors
  if (network.available()) {
    RF24NetworkHeader header;
    network.peek(header);

    uint32_t dat = 0;
    int id_node = 0;
    switch (header.type) {
      // Display the incoming millis() values from the sensor nodes
      case 'M':
        network.read(header, &dat, sizeof(dat));
        id_node = mesh.getNodeID(header.from_node);
        Serial.print("master - from_node: ");
        Serial.print(header.from_node);
        Serial.print(" to_node: ");
        Serial.print(header.to_node);
        Serial.print(" id_node: ");
        Serial.print(id_node);
        Serial.print(" id: ");
        Serial.print(header.id);
        Serial.print(" type: ");
        Serial.print(header.type);
        Serial.print(" ");
        Serial.println(dat);
        break;

      case 'E':
        network.read(header, &dat, sizeof(dat));
        Serial.println(dat);
        break;

      default:
        network.read(header, 0, 0);
        Serial.println(header.type);
        break;
    }
  }

  // Si paso el tiempo muestra la info.
  if (timeElapsed > interval) {
    counter ++;
    Serial.println(" ");
    Serial.println(F("master - ********Assigned Addresses********"));
    for (int i = 0; i < mesh.addrListTop; i++) {
      int nodeID = mesh.addrList[i].nodeID;
      Serial.print("master - NodeID: ");
      Serial.print(nodeID);
      Serial.print(" RF24Network Address: 0");
      uint16_t address = mesh.addrList[i].address;
      Serial.print(address, OCT);

      // Envia un mensaje a cada nodo.
      payload_t payload;
      payload.ms = millis();
      payload.counter = counter ;
      // RF24NetworkHeader header(mesh.addrList[i].address, OCT); //Constructing a header
      // network.write(header, &message, sizeof(message));
      //if (network.write(header, &payload, sizeof(payload)) == 1) {
      if (mesh.write(&payload, 'T', sizeof(payload), nodeID)) {
        Serial.println( F( " - Send OK")); //Sending an message
      } else {
        Serial.println( F(  " - Send Fail")); //Sending an message
      }
    }

    Serial.println(F("master - **********************************"));
    timeElapsed = 0;
  }
}
